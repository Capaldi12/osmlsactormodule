﻿using System;

namespace ActorModule
{
    /// <summary>
    /// Активность, которую можно задать актору
    /// </summary>
    public interface IActivity
    {
        /// <summary>
        /// Приоритет активности. Используется для сравнения важности активностей
        /// </summary>
        public int Priority { get; }
        // Приватные члены в интерфейсе не объявляются, поэтому просто убрал сеттер
        // В свои имплементации просто добавьте private set

        /// <summary>
        /// Обновление активности. В этом методе необходимо выполнять все действия<br/>
        /// Должен возвращать True, если активность завершена и может быть удалена
        /// </summary>
        /// <param name="actor">Актор, на котором вызывается активность</param>
        /// <param name="deltaTime">Время с предыдущего обновления, в секундах</param>
        /// <returns>True, если активность завершена</returns>
        public bool Update(Actor actor, double deltaTime);
    }
}
