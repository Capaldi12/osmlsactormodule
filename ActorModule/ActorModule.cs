﻿using System;
using System.Collections.Generic;

using OSMLSGlobalLibrary.Map;
using OSMLSGlobalLibrary.Modules;

using NetTopologySuite.Geometries;

namespace ActorModule
{
    /// <summary>
    /// Основной класс модуля. Отвечает за вызовы Update у всех акторов в MapObjects
    /// </summary>
    public class ActorModule : OSMLSModule  
    {
        private long lastElapsed = 0;

#if DEBUG
        private double vibeTime = 0;
#endif

        /// <summary>
        /// Инициализация модуля. В отладочной конфигурации выводит сообщение
        /// </summary>
        protected override void Initialize() { }

        /// <summary>
        /// Вызывает Update на всех акторах
        /// </summary>
        /// <param name="elapsedMilliseconds">Время с инициализации модуля?</param>
        public override void Update(long elapsedMilliseconds)
        {
            // Вычисляем время с последнего апдейта - в секундах
            double deltaTime = (double)(elapsedMilliseconds - lastElapsed) / 1000;
            lastElapsed = elapsedMilliseconds;

            foreach (Actor actor in MapObjects.GetAll<Actor>())
                actor.Update(deltaTime);
#if DEBUG
            vibeTime -= deltaTime;
            if (vibeTime < 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;

                Console.WriteLine($"\nVibe Check");
                Console.WriteLine($"Server time: {DateTime.Now:HH:mm:ss UTCz}");
                Console.WriteLine($"Around {1 / deltaTime:0.#} updates per second");
                Console.WriteLine($"{deltaTime:0.##}s since last update");

                vibeTime = 1;

                foreach (Actor actor in MapObjects.GetAll<Actor>())
                    actor.vibe();

                Console.WriteLine();

                Console.ResetColor();
            }
#endif
        }
    }
}
